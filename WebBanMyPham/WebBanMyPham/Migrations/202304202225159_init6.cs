namespace WebBanMyPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ProductImage", "Image", c => c.String());
            AlterColumn("dbo.ProductImage", "IsDefault", c => c.Boolean(nullable: false));
            CreateIndex("dbo.ProductImage", "ProductId");
            AddForeignKey("dbo.ProductImage", "ProductId", "dbo.Product", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductImage", "ProductId", "dbo.Product");
            DropIndex("dbo.ProductImage", new[] { "ProductId" });
            AlterColumn("dbo.ProductImage", "IsDefault", c => c.Int(nullable: false));
            AlterColumn("dbo.ProductImage", "Image", c => c.Int(nullable: false));
        }
    }
}
