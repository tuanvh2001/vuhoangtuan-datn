namespace WebBanMyPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init9 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderDetail", "Order_Id", "dbo.Order");
            DropIndex("dbo.OrderDetail", new[] { "Order_Id" });
            DropColumn("dbo.OrderDetail", "OrderId");
            RenameColumn(table: "dbo.OrderDetail", name: "Order_Id", newName: "OrderId");
            AlterColumn("dbo.OrderDetail", "OrderId", c => c.Int(nullable: false));
            AlterColumn("dbo.OrderDetail", "OrderId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderDetail", "OrderId");
            AddForeignKey("dbo.OrderDetail", "OrderId", "dbo.Order", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderDetail", "OrderId", "dbo.Order");
            DropIndex("dbo.OrderDetail", new[] { "OrderId" });
            AlterColumn("dbo.OrderDetail", "OrderId", c => c.Int());
            AlterColumn("dbo.OrderDetail", "OrderId", c => c.String());
            RenameColumn(table: "dbo.OrderDetail", name: "OrderId", newName: "Order_Id");
            AddColumn("dbo.OrderDetail", "OrderId", c => c.String());
            CreateIndex("dbo.OrderDetail", "Order_Id");
            AddForeignKey("dbo.OrderDetail", "Order_Id", "dbo.Order", "Id");
        }
    }
}
