namespace WebBanMyPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "OriginalPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.Category", "SeoTitle");
            DropColumn("dbo.Category", "SeoDescription");
            DropColumn("dbo.Category", "SeoKeywords");
            DropColumn("dbo.News", "SeoTitle");
            DropColumn("dbo.News", "SeoDescription");
            DropColumn("dbo.News", "SeoKeywords");
            DropColumn("dbo.Posts", "SeoTitle");
            DropColumn("dbo.Posts", "SeoDescription");
            DropColumn("dbo.Posts", "SeoKeywords");
            DropColumn("dbo.Product", "SeoTitle");
            DropColumn("dbo.Product", "SeoDescription");
            DropColumn("dbo.Product", "SeoKeywords");
            DropColumn("dbo.ProductCategory", "SeoTitle");
            DropColumn("dbo.ProductCategory", "SeoDescription");
            DropColumn("dbo.ProductCategory", "SeoKeywords");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductCategory", "SeoKeywords", c => c.String());
            AddColumn("dbo.ProductCategory", "SeoDescription", c => c.String());
            AddColumn("dbo.ProductCategory", "SeoTitle", c => c.String());
            AddColumn("dbo.Product", "SeoKeywords", c => c.String());
            AddColumn("dbo.Product", "SeoDescription", c => c.String());
            AddColumn("dbo.Product", "SeoTitle", c => c.String());
            AddColumn("dbo.Posts", "SeoKeywords", c => c.String());
            AddColumn("dbo.Posts", "SeoDescription", c => c.String());
            AddColumn("dbo.Posts", "SeoTitle", c => c.String());
            AddColumn("dbo.News", "SeoKeywords", c => c.String());
            AddColumn("dbo.News", "SeoDescription", c => c.String());
            AddColumn("dbo.News", "SeoTitle", c => c.String());
            AddColumn("dbo.Category", "SeoKeywords", c => c.String());
            AddColumn("dbo.Category", "SeoDescription", c => c.String());
            AddColumn("dbo.Category", "SeoTitle", c => c.String());
            DropColumn("dbo.Product", "OriginalPrice");
        }
    }
}
