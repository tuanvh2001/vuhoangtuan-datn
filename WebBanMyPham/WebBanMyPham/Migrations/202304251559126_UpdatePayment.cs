namespace WebBanMyPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdatePayment : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "TypePayment", c => c.Int(nullable: false));
            AlterColumn("dbo.Order", "Quantity", c => c.Int(nullable: false));
            AlterColumn("dbo.Order", "TotalAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Order", "TotalAmount", c => c.String());
            AlterColumn("dbo.Order", "Quantity", c => c.String());
            DropColumn("dbo.Order", "TypePayment");
        }
    }
}
