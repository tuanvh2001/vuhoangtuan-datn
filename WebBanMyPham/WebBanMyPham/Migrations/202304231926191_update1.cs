namespace WebBanMyPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ShoppingCartItems", "Product_Id", "dbo.Product");
            DropIndex("dbo.ShoppingCartItems", new[] { "Product_Id" });
            DropTable("dbo.ShoppingCartItems");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ShoppingCartItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductName = c.String(),
                        Alias = c.String(),
                        CategorytName = c.String(),
                        ProductImg = c.String(),
                        Image = c.String(),
                        Quantity = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Product_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.ShoppingCartItems", "Product_Id");
            AddForeignKey("dbo.ShoppingCartItems", "Product_Id", "dbo.Product", "Id");
        }
    }
}
