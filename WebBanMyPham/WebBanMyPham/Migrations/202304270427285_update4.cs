namespace WebBanMyPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Order", "Code", c => c.String());
            DropColumn("dbo.Order", "OrderId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Order", "OrderId", c => c.String());
            DropColumn("dbo.Order", "Code");
        }
    }
}
