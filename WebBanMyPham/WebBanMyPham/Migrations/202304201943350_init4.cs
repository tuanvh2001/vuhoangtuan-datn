namespace WebBanMyPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductImage",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductId = c.Int(nullable: false),
                        Image = c.Int(nullable: false),
                        IsDefault = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Category", "Alias", c => c.String());
            AddColumn("dbo.News", "Alias", c => c.String());
            AddColumn("dbo.Posts", "Alias", c => c.String());
            AddColumn("dbo.Product", "Alias", c => c.String());
            AddColumn("dbo.ProductCategory", "Icon", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductCategory", "Icon");
            DropColumn("dbo.Product", "Alias");
            DropColumn("dbo.Posts", "Alias");
            DropColumn("dbo.News", "Alias");
            DropColumn("dbo.Category", "Alias");
            DropTable("dbo.ProductImage");
        }
    }
}
