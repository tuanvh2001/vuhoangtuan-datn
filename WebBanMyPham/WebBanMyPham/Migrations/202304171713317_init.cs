namespace WebBanMyPham.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Category", "SeoKeywords", c => c.String());
            AlterColumn("dbo.News", "SeoKeywords", c => c.String());
            AlterColumn("dbo.Posts", "SeoKeywords", c => c.String());
            AlterColumn("dbo.Product", "SeoKeywords", c => c.String());
            AlterColumn("dbo.ProductCategory", "SeoKeywords", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductCategory", "SeoKeywords", c => c.Int(nullable: false));
            AlterColumn("dbo.Product", "SeoKeywords", c => c.Int(nullable: false));
            AlterColumn("dbo.Posts", "SeoKeywords", c => c.Int(nullable: false));
            AlterColumn("dbo.News", "SeoKeywords", c => c.Int(nullable: false));
            AlterColumn("dbo.Category", "SeoKeywords", c => c.Int(nullable: false));
        }
    }
}
