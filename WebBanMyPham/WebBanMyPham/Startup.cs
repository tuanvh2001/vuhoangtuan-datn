﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebBanMyPham.Startup))]
namespace WebBanMyPham
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
