﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebBanMyPham.Models.EF
{
    [Table("Product")]
    public class Product:CommonAbstract
    {

        public Product()
        {
            this.OrderDetails = new HashSet<OrderDetail>();
           

        }
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string ProductId { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public string Descrption { get; set; }
        public string Detail { get; set; }
        public decimal OriginalPrice { get; set; }
        public string Image { get; set; }
        public decimal Price { get; set; }
        public decimal? PriceSale { get; set; }
        public int Quantity { get; set; }
        public bool IsHome { get; set; }
        public bool IsSale{ get; set; }
        public bool IsHot { get; set; }
        public bool IsFeature { get; set; }
        public bool IsActive { get; set; }
        public int ProductCategoryId { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
       
      

    }
}