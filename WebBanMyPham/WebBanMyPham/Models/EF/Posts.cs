﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebBanMyPham.Models.EF
{
    [Table("Posts")]
    public class Posts:CommonAbstract
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Alias { get; set; }
        public string Descrption { get; set; }
       
        public string Detail { get; set; }
        public string Image { get; set; }
        public int CategoryId { get; set; }
        public bool IsActive { get; set; }
        public virtual Category Category { get; set; }
    }
}